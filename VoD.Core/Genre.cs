﻿using System.Collections.Generic;

namespace VoD.Core
{
    public class Genre
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }
    }
}
