﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoD.Core
{
    public class Movie
    {
        // exception with primary key virtual: http://stackoverflow.com/a/12134492
        public int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual int Year { get; set; }

        public virtual ICollection<Actor> Cast { get; set; }
        public virtual ICollection<Director> Directors { get; set; }
        public virtual ICollection<Genre> Genres { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
