﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VoD.Core
{
    public class Comment
    {
        public virtual int Id { get; set; }
        public virtual Guid AuthorGuid { get; set; }
        public virtual string Body { get; set; }
        public virtual int Rate { get; set; }
        public virtual int MovieId { get; set; }
        public virtual Movie Movie { get; set; }
    }
}
