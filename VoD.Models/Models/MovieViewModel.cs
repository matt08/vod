﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VoD.Core;

namespace VoD.Website.Models
{
    public class MovieViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Year { get; set; }

        public ICollection<Actor> Cast { get; set; }
        public ICollection<Director> Directors { get; set; }
        public ICollection<Genre> Genres { get; set; }
        public ICollection<CommentViewModel> Comments { get; set; }
    }
}