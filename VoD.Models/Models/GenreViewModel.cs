﻿using System.ComponentModel.DataAnnotations;

namespace VoD.Models
{
    public class GenreViewModel
    {
        [Required]
        public string Name { get; set; }
    }
}
