﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VoD.Core;

namespace VoD.Website.Models
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        public string Author { get; set; }
        public string Body { get; set; }
        public int Rate { get; set; }
    }
}