﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace VoD.Website.Models
{
    public class AddMovieViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name="Tytuł")]
        public string Title { get; set; }
        [Required]
        [Display(Name = "Opis")]
        public string Description { get; set; }
        [Required]
        [Range(1800, 2030)]
        [Display(Name = "Rok")]
        public int Year { get; set; }

        [Display(Name = "Obsada")]
        public IEnumerable<SelectListItem> Cast { get; set; }
        [Display(Name = "Reżyseria")]
        public IEnumerable<SelectListItem> Directors { get; set; }
        [Display(Name = "Gatunek")]
        public IEnumerable<SelectListItem> Genres { get; set; }

        // for selection, add custom attributes, cannot be empty
        public List<int> CastId { get; set; }
        public List<int> DirectorsId { get; set; }
        public List<int> GenresId { get; set; }

        [Display(Name = "Okładka")]
        [DataType(DataType.Upload)]
        public HttpPostedFileBase MovieImage { get; set; }

        public AddMovieViewModel()
        {
            CastId = new List<int>();
            DirectorsId = new List<int>();
            GenresId = new List<int>();
        }

    }
}