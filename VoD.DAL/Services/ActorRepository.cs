﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using VoD.Core;
using VoD.DAL.DataContexts;
using VoD.DAL.Services.Interfaces;

namespace VoD.DAL.Services
{
    public class ActorRepository : IActorRepository
    {
        private readonly MovieDb context = new MovieDb();

        public Actor GetById(int id)
        {
            var actor = context.Actors.Find(id);
            return actor;
        }

        public void Add(Actor item)
        {
            context.Actors.Add(item);
            context.SaveChanges();
        }

        public void RemoveById(int id)
        {
            var actor = new Actor { Id = id };
            context.Actors.Attach(actor);
            context.Actors.Remove(actor);
            context.SaveChanges();
        }

        public void Update(Actor item)
        {
            context.Actors.Attach(item);
            context.Entry(item).State = EntityState.Modified;
            context.SaveChanges();
        }

        public List<Actor> GetAll()
        {
            var actorsList = context.Actors.ToList();
            return actorsList;
        }
    }
}