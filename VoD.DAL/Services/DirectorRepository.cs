﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using VoD.Core;
using VoD.DAL.DataContexts;
using VoD.DAL.Services.Interfaces;

namespace VoD.DAL.Services
{
    public class DirectorRepository : IDirectorRepository
    {
        private readonly MovieDb context = new MovieDb();

        public Director GetById(int id)
        {
            var director = context.Directors.Find(id);
            return director;
        }

        public void Add(Director item)
        {
            context.Directors.Add(item);
            context.SaveChanges();
        }

        public void RemoveById(int id)
        {
            var director = new Director { Id = id };
            context.Directors.Attach(director);
            context.Directors.Remove(director);
            context.SaveChanges();
        }

        public void Update(Director item)
        {
            context.Directors.Attach(item);
            context.Entry(item).State = EntityState.Modified;
            context.SaveChanges();
        }

        public List<Director> GetAll()
        {
            var directorsList = context.Directors.ToList();
            return directorsList;
        }
    }
}