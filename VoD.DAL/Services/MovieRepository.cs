﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VoD.Core;
using VoD.DAL.DataContexts;
using System.Data.Entity;
using VoD.Website.Models;
using System.Web.Mvc;
using VoD.DAL;
using VoD.DAL.Services.Interfaces;
namespace VoD.DAL.Services
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MovieDb context = new MovieDb();

        public List<Movie> GetAllMoviesWithDirectors()
        {
            var moviesList = context.Movies.Include(m => m.Directors).ToList();
            return moviesList;
        }

        public Movie GetById(int id)
        {
            var movie = context.Movies.Single(m => m.Id == id);
            return movie;
        }

        public void Add(Movie item)
        {
            context.Movies.Add(item);
            context.SaveChanges();
        }

        public void AddFromViewModel(AddMovieViewModel movie)
        {
            Movie newMovie = new Movie();
            newMovie.Title = movie.Title;
            newMovie.Description = movie.Description;
            newMovie.Year = movie.Year;
            newMovie.Cast = new List<Actor>();
            newMovie.Directors = new List<Director>();
            newMovie.Genres = new List<Genre>();

            context.Movies.Add(newMovie);

            foreach (var newActorId in movie.CastId)
            {
                var newActor = new Actor { Id = newActorId };
                context.Actors.Attach(newActor);
                newMovie.Cast.Add(newActor);
            }

            foreach (var newDirectorId in movie.DirectorsId)
            {
                var newDirector = new Director { Id = newDirectorId };
                context.Directors.Attach(newDirector);
                newMovie.Directors.Add(newDirector);
            }

            foreach (var newGenreId in movie.GenresId)
            {
                var newGenre = new Genre { Id = newGenreId };
                context.Genres.Attach(newGenre);
                newMovie.Genres.Add(newGenre);
            }

            context.SaveChanges();
        }

        public void RemoveById(int id)
        {
            var movie = new Movie { Id = id };
            context.Movies.Attach(movie);
            context.Movies.Remove(movie);
            context.SaveChanges();
        }

        public void Update(Movie item)
        {
            throw new NotImplementedException();
        }


        public void UpdateFromViewModel(AddMovieViewModel movie)
        {
            var movieToEdit = context.Movies
                .Include(m => m.Cast)
                .Include(m => m.Directors)
                .Include(m => m.Genres)
                .Single(m => m.Id == movie.Id);
            movieToEdit.Title = movie.Title;
            movieToEdit.Description = movie.Description;
            movieToEdit.Year = movie.Year;

            foreach(var actor in movieToEdit.Cast.ToList())
            {
                if(!movie.CastId.Contains(actor.Id))
                {
                    movieToEdit.Cast.Remove(actor);
                }
            }

            foreach(var newActorId in movie.CastId)
            {
                if(!movieToEdit.Cast.Any(c=>c.Id==newActorId))
                {
                    var newActor = new Actor { Id = newActorId };
                    context.Actors.Attach(newActor);
                    movieToEdit.Cast.Add(newActor);
                }
            }

            foreach(var director in movieToEdit.Directors.ToList())
            {
                if(!movie.DirectorsId.Contains(director.Id))
                {
                    movieToEdit.Directors.Remove(director);
                }
            }

            foreach(var newDirectorId in movie.DirectorsId)
            {
                if(!movieToEdit.Directors.Any(d=>d.Id==newDirectorId))
                {
                    var newDirector = new Director { Id = newDirectorId };
                    context.Directors.Attach(newDirector);
                    movieToEdit.Directors.Add(newDirector);
                }
            }

            foreach(var genre in movieToEdit.Genres.ToList())
            {
                if(!movie.GenresId.Contains(genre.Id))
                {
                    movieToEdit.Genres.Remove(genre);
                }
            }

            foreach(var newGenreId in movie.GenresId)
            {
                if(!movieToEdit.Genres.Any(g=>g.Id==newGenreId))
                {
                    var newGenre = new Genre { Id = newGenreId };
                    context.Genres.Attach(newGenre);
                    movieToEdit.Genres.Add(newGenre);
                }
            }

            context.SaveChanges();
        }


        public List<LatestMovieViewModel> GetLatestMovies()
        {
            var latestMovies = context.Movies
                .OrderByDescending(m => m.Id)
                .Take(3)
                .Select(m => new LatestMovieViewModel
                {
                    Id = m.Id,
                    Title = m.Title,
                    Description = m.Description
                }).ToList();

            return latestMovies;
        }


        public List<Movie> SearchMovies(string title)
        {
            var foundMovies = context.Movies
                .Include(m => m.Directors)
                .Include(m => m.Genres)
                .Where(m => m.Title.Contains(title))
                .ToList();

            return foundMovies;
        }


        public void AddCommentToMovie(Comment newComment)
        {
            context.Comments.Add(newComment);
            context.SaveChanges();
        }


        public void RemoveCommentFromMovie(int commentId, int movieId)
        {
            var commentToRemove = new Comment { Id = commentId, MovieId = movieId };
            context.Comments.Attach(commentToRemove);
            context.Comments.Remove(commentToRemove);
            context.SaveChanges();
        }


        public Movie GetMovieDetails(int movieId)
        {
            var movieDetails = context.Movies
                .Where(m => m.Id == movieId)
                .Include(m => m.Cast)
                .Include(m => m.Comments)
                .Include(m => m.Directors)
                .Include(m => m.Genres)
                .FirstOrDefault();

            return movieDetails;
        }


        public MovieViewModel GetMovieViewModelDetails(int movieId)
        {
            var movieDetails = context.Movies
                .Where(m => m.Id == movieId)
                .Include(m => m.Cast)
                .Include(m => m.Comments)
                .Include(m => m.Directors)
                .Include(m => m.Genres)
                .FirstOrDefault();

            var movieViewModel = new MovieViewModel();
            movieViewModel.Id = movieDetails.Id;
            movieViewModel.Title = movieDetails.Title;
            movieViewModel.Description = movieDetails.Description;
            movieViewModel.Year = movieDetails.Year;
            movieViewModel.Cast = movieDetails.Cast;

            // comments, get username
            using (var db = new ApplicationDbContext())
            {
                movieViewModel.Comments = movieDetails.Comments
                    .Select(
                    c =>
                    {
                        var guid = c.AuthorGuid.ToString();
                        return new CommentViewModel
                        {
                            Id = c.Id,
                            Body = c.Body,
                            Rate = c.Rate,
                            Author = db.Users.Where(u => u.Id == guid).FirstOrDefault().UserName
                        };
                    }).ToList();
            }

            movieViewModel.Directors = movieDetails.Directors;
            movieViewModel.Genres = movieDetails.Genres;

            return movieViewModel;
            
        }

        public IEnumerable<SelectListItem> GetGenresSelectList()
        {
            var genresSelectList = new SelectList
                (
                    context.Genres.OrderBy(genre => genre.Name)
                    .ToList()
                    .Select(genre => new SelectListItem
                    {
                        Selected = false,
                        Text = genre.Name,
                        Value = genre.Id.ToString()
                    })
                , "Value", "Text");

            return genresSelectList;
        }

        public IEnumerable<SelectListItem> GetCastSelectList()
        {
            var castSelectList = new SelectList(
                context.Actors
                .OrderBy(actor => actor.LastName)
                .ToList()
                .Select(actor => new SelectListItem
                {
                    Selected = false,
                    Text = String.Format("{0} {1}", actor.FirstName, actor.LastName),
                    Value = actor.Id.ToString()
                })
                , "Value", "Text");

            return castSelectList;
        }

        public IEnumerable<SelectListItem> GetDirectorsSelectList()
        {
            var directorsSelectList = new SelectList(
                context.Directors
                .OrderBy(director => director.LastName)
                .ToList()
                .Select(director => new SelectListItem
                {
                    Selected = false,
                    Text = String.Format("{0} {1}", director.FirstName, director.LastName),
                    Value = director.Id.ToString()
                })
                , "Value", "Text");

            return directorsSelectList;
        }

    }
}