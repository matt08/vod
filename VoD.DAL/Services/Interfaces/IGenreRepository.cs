﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoD.Core;
using VoD.Website.Models;

namespace VoD.DAL.Services.Interfaces
{
    public interface IGenreRepository : IRepository<Genre>
    {
        List<Genre> GetAll();
        List<CategoryViewModel> GetAllGenresWithMovieCount();
        List<MoviesFromCategoryViewModel> GetMoviesFromCategory(int categoryId);
    }
}
