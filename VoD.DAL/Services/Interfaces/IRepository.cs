﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VoD.DAL.Services.Interfaces
{
    public interface IRepository<T>
    {
        T GetById(int id);
        void Add(T item);
        void RemoveById(int id);
        void Update(T item);
    }
}
