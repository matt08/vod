﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using VoD.Core;
using VoD.Website.Models;

namespace VoD.DAL.Services.Interfaces
{
    public interface IMovieRepository : IRepository<Movie>
    {
        List<Movie> GetAllMoviesWithDirectors();
        List<LatestMovieViewModel> GetLatestMovies();
        List<Movie> SearchMovies(string title);
        void AddCommentToMovie(Comment newComment);
        void RemoveCommentFromMovie(int commentId, int movieId);
        Movie GetMovieDetails(int movieId);
        MovieViewModel GetMovieViewModelDetails(int movieId);
        IEnumerable<SelectListItem> GetGenresSelectList();
        IEnumerable<SelectListItem> GetCastSelectList();
        IEnumerable<SelectListItem> GetDirectorsSelectList();

        void AddFromViewModel(AddMovieViewModel movie);
        void UpdateFromViewModel(AddMovieViewModel movie);
    }
}
