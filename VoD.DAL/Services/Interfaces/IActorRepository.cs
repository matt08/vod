﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VoD.Core;

namespace VoD.DAL.Services.Interfaces
{
    public interface IActorRepository : IRepository<Actor>
    {
        List<Actor> GetAll();
    }
}
