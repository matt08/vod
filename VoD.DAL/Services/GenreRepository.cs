﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using VoD.Core;
using VoD.DAL.DataContexts;
using VoD.DAL.Services.Interfaces;
using VoD.Website.Models;

namespace VoD.DAL.Services
{
    public class GenreRepository : IGenreRepository
    {
        private readonly MovieDb context = new MovieDb();

        public Genre GetById(int id)
        {
            var genre = context.Genres.Find(id);
            return genre;
        }

        public void Add(Genre item)
        {
            context.Genres.Add(item);
            context.SaveChanges();
        }

        public void RemoveById(int id)
        {
            var genre = new Genre { Id = id };
            context.Genres.Attach(genre);
            context.Genres.Remove(genre);
            context.SaveChanges();
        }

        public void Update(Genre item)
        {
            context.Genres.Attach(item);
            context.Entry(item).State = EntityState.Modified;
            context.SaveChanges();
        }

        public List<Genre> GetAll()
        {
            var genresList = context.Genres.ToList();
            return genresList;
        }


        public List<CategoryViewModel> GetAllGenresWithMovieCount()
        {
            var genresList = context.Genres
                .Include(m => m.Movies)
                .Select(g => new CategoryViewModel
                {
                    Id = g.Id,
                    Name = g.Name,
                    Count = g.Movies.Count
                })
                .ToList();

            return genresList;
        }

        public List<MoviesFromCategoryViewModel> GetMoviesFromCategory(int categoryId)
        {
            var moviesFromCategory = context.Movies
                .Where(m => m.Id == categoryId)
                .Select(m => new MoviesFromCategoryViewModel
                {
                    Id = m.Id,
                    Title = m.Title,
                    Description = m.Description,
                    Year = m.Year
                })
            .ToList();

            return moviesFromCategory;
        }
    }
}