namespace VoD.DAL.DataContexts.MovieMigrations
{
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using VoD.Core;

    internal sealed class Configuration : DbMigrationsConfiguration<VoD.DAL.DataContexts.MovieDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            MigrationsDirectory = @"DataContexts\MovieMigrations";
        }

        protected override void Seed(VoD.DAL.DataContexts.MovieDb context)
        {
            context.Movies.AddOrUpdate(
                m => m.Title,
                    new Movie
                    {
                        Title = "Piraci z Karaib�w: Kl�twa Czarnej Per�y",
                        Description = "Kowal Will Turner sprzymierza si� z kapitanem Jackiem Sparrowem, by odzyska� swoj� mi�o�� - porwan� c�rk� gubernatora.",
                        Directors = new List<Director> { new Director { FirstName= "Gore", LastName="Verbinski" } },
                        Genres = new List<Genre> { new Genre { Name = "Adventure" } },
                        Year = 2003,
                        Cast = new List<Actor> { new Actor { FirstName = "Johnny", LastName = "Depp" } },
                    },
                    new Movie
                    {
                        Title = "Titanic",
                        Description = "Rok 1912, brytyjski statek Titanic wyrusza w sw�j dziewiczy rejs do USA. Na pok�adzie emigrant Jack przypadkowo spotyka arystokratyczn� Rose.",
                        Directors = new List<Director> { new Director { FirstName = "James", LastName = "Cameron" } },
                        Genres = new List<Genre> { new Genre { Name = "Action" } },
                        Year = 1997,
                        Cast = new List<Actor> { new Actor { FirstName = "Leonardo", LastName = "DiCaprio" } },
                    },
                    new Movie
                    {
                        Title = "Sz�sty zmys�",
                        Description = "Psycholog dzieci�cy pr�buje pom�c o�mioletniemu ch�opcu widz�cemu zmar�ych poradzi� sobie z tym niezwyk�ym darem.",
                        Directors = new List<Director> { new Director { FirstName = "M. Night", LastName = "Shaymalan" } },
                        Genres = new List<Genre> { new Genre { Name = "Thriller" } },
                        Year = 1999,
                        Cast = new List<Actor> { new Actor { FirstName = "Bruce", LastName = "Willis" } },
                    },
                    new Movie
                    {
                        Title = "Slumdog. Milioner z ulicy",
                        Description = "Opowie�� o m�odym ch�opaku, kt�ry bierze udzia� w hinduskiej edycji \"Milioner�w\".",
                        Directors = new List<Director> { new Director { FirstName = "Danny", LastName = "Boyle" } },
                        Genres = new List<Genre> { new Genre { Name = "Drama" } },
                        Year = 2008,
                        Cast = new List<Actor> { new Actor { FirstName = "Dev", LastName = "Patel" } },
                    }
                );
        }
    }
}
