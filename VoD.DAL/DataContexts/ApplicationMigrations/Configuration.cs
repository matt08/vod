namespace VoD.DAL.DataContexts.ApplicationMigrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<VoD.DAL.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            MigrationsDirectory = @"DataContexts\ApplicationMigrations";
        }

        protected override void Seed(VoD.DAL.ApplicationDbContext context)
        {
            if (!context.Users.Any(u => u.UserName=="admin" || u.UserName=="user"))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                var adminUser = new ApplicationUser { UserName = "admin" };
                var user = new ApplicationUser { UserName = "user" };
                userManager.Create(adminUser, "qwerty");
                userManager.Create(user, "qwerty");

                var roleStore = new RoleStore<IdentityRole>(context);
                var roleManager = new RoleManager<IdentityRole>(roleStore);
                roleManager.Create(new IdentityRole { Name = "Administrator" });
                userManager.AddToRole(adminUser.Id, "Administrator");
            }
        }
    }
}
