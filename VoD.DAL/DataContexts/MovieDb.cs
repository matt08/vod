﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using VoD.Core;

namespace VoD.DAL.DataContexts
{
    public class MovieDb : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Actor> Actors { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Director> Directors { get; set; }

        public MovieDb() : base("DefaultConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Actor>().Property(p => p.FirstName).IsRequired();
            modelBuilder.Entity<Actor>().Property(p => p.LastName).IsRequired();

            modelBuilder.Entity<Comment>().Property(p => p.AuthorGuid).IsRequired();
            modelBuilder.Entity<Comment>().Property(p => p.Body).IsRequired();
            modelBuilder.Entity<Comment>().Property(p => p.MovieId).IsRequired();
            modelBuilder.Entity<Comment>().Property(p => p.Rate).IsRequired();

            modelBuilder.Entity<Director>().Property(p => p.FirstName).IsRequired();
            modelBuilder.Entity<Director>().Property(p => p.LastName).IsRequired();

            modelBuilder.Entity<Genre>().Property(p => p.Name).IsRequired();

            modelBuilder.Entity<Movie>().Property(p => p.Title).IsRequired();
            modelBuilder.Entity<Movie>().Property(p => p.Description).IsRequired();
            modelBuilder.Entity<Movie>().Property(p => p.Year).IsRequired();
            modelBuilder.Entity<Movie>().HasMany(a => a.Comments).WithRequired(a => a.Movie).WillCascadeOnDelete(true);

            base.OnModelCreating(modelBuilder);
        }
    }
}