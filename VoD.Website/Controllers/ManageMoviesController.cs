﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoD.Core;
using VoD.DAL.Services.Interfaces;
using VoD.Website.Models;


namespace VoD.Website.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ManageMoviesController : Controller
    {
        private readonly IMovieRepository repository;

        public ManageMoviesController(IMovieRepository repository)
        {
            this.repository = repository;
        }

        //
        // GET: /ManageMovies/
        public ActionResult Index()
        {
            var moviesList = repository.GetAllMoviesWithDirectors();
            return View(moviesList);
        }

        [HttpGet]
        public ActionResult AddMovie()
        {
            var newMovie = new AddMovieViewModel();
            // Populate genres, actors, directors available to select
            newMovie.Genres = repository.GetGenresSelectList();
            newMovie.Cast = repository.GetCastSelectList();
            newMovie.Directors = repository.GetDirectorsSelectList();

            return View(newMovie);
        }

        [HttpPost]
        public ActionResult AddMovie(AddMovieViewModel movie)
        {
            if (ModelState.IsValid)
            {
                repository.AddFromViewModel(movie);
                return RedirectToAction("Index");
            }
            else
            {
                // repopulate collections avaiable to select, selected ids are kept in CastId collection
                // what is Selected property in SelectListItem for?
                movie.Cast = repository.GetCastSelectList();
                movie.Directors = repository.GetDirectorsSelectList();
                movie.Genres = repository.GetGenresSelectList();
                return View(movie);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveMovie(int movieId)
        {
            repository.RemoveById(movieId);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditMovie(int movieId)
        {
            var movie = repository.GetById(movieId);

            // populate view model and send
            var movieViewModel = new AddMovieViewModel();
            movieViewModel.Id = movie.Id;
            movieViewModel.Title = movie.Title;
            movieViewModel.Description = movie.Description;
            movieViewModel.Year = movie.Year;

            // does not select without it
            movieViewModel.GenresId = movie.Genres.Select(a => a.Id).ToList();
            movieViewModel.CastId = movie.Cast.Select(a => a.Id).ToList();
            movieViewModel.DirectorsId = movie.Directors.Select(a => a.Id).ToList();

            movieViewModel.Cast = repository.GetCastSelectList();
            movieViewModel.Directors = repository.GetDirectorsSelectList();
            movieViewModel.Genres = repository.GetGenresSelectList();

            return View(movieViewModel);
        }

        [HttpPost]
        public ActionResult EditMovie(AddMovieViewModel movie)
        {
            var validImageTypes = new string[]
             {
                 "image/jpeg",
                 "image/pjpeg",
             };

            if (movie.MovieImage != null && !validImageTypes.Contains(movie.MovieImage.ContentType))
            {
                ModelState.AddModelError("MovieImage", "Please choose either a JPG image.");
            }

            if (ModelState.IsValid)
            {
                repository.UpdateFromViewModel(movie);

                if (movie.MovieImage != null)
                {
                    var uploadDir = "~/img/movies/";
                    var imagePath = Path.Combine(Server.MapPath(uploadDir), String.Format("{0}.jpg", movie.Id));
                    movie.MovieImage.SaveAs(imagePath);
                }

                return RedirectToAction("Index");
            }

            return RedirectToAction("EditMovie", new { movieId = movie.Id });
        }
	}
}