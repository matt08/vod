﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoD.Core;
using VoD.DAL.Services.Interfaces;
using VoD.Models;

namespace VoD.Website.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ManageGenresController : Controller
    {
        private readonly IGenreRepository repository;

        public ManageGenresController(IGenreRepository repository)
        {
            this.repository = repository;
        }

        //
        // GET: /ManageGenres/
        public ActionResult Index()
        {
            var genresList = repository.GetAll();
            return View(genresList);
        }

        [HttpGet]
        public ActionResult AddGenre()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddGenre(GenreViewModel genreViewModel)
        {
            if (ModelState.IsValid)
            {
                Genre genre = Mapper.Map<GenreViewModel, Genre>(genreViewModel);
                repository.Add(genre);
                return RedirectToAction("Index");
            }

            return View(genreViewModel);
        }

        [HttpGet]
        public ActionResult EditGenre(int genreId)
        {
            var genre = repository.GetById(genreId);
            return View(genre);
        }

        [HttpPost]
        public ActionResult EditGenre(Genre genre)
        {
            if (ModelState.IsValid)
            {
                repository.Update(genre);
                return RedirectToAction("Index");
            }

            return RedirectToAction("EditGenre", new { genreId = genre.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveGenre(int genreId)
        {
            repository.RemoveById(genreId);
            return RedirectToAction("Index");
        }
	}
}