﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoD.Core;
using VoD.DAL.Services.Interfaces;
using VoD.Website.Models;


namespace VoD.Website.Controllers
{
    public class CategoryController : Controller
    {
        private readonly IGenreRepository repository;

        public CategoryController(IGenreRepository repository)
        {
            this.repository = repository;
        }

        //
        // GET: /Category/
        public ActionResult Index()
        {
            var genresList = repository.GetAllGenresWithMovieCount();
            return View(genresList);
        }

        public ActionResult ViewCategory(int categoryId)
        {
            var moviesFromCategory = repository.GetMoviesFromCategory(categoryId);
            return View(moviesFromCategory);
        }
	}
}