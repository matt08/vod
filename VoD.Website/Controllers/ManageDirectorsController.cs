﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoD.Core;
using VoD.DAL.Services.Interfaces;

namespace VoD.Website.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ManageDirectorsController : Controller
    {
        private readonly IDirectorRepository repository;

        public ManageDirectorsController(IDirectorRepository repository)
        {
            this.repository = repository;
        }

        //
        // GET: /ManageDirectors/
        public ActionResult Index()
        {
            var directorsList = repository.GetAll();
            return View(directorsList);
        }

        [HttpGet]
        public ActionResult AddDirector()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddDirector(Director director)
        {
            if (ModelState.IsValid)
            {
                repository.Add(director);
                return RedirectToAction("Index");
            }

            return View(director);
        }

        [HttpGet]
        public ActionResult EditDirector(int directorId)
        {
            var director = repository.GetById(directorId);
            return View(director);
        }

        [HttpPost]
        public ActionResult EditDirector(Director director)
        {
            if (ModelState.IsValid)
            {
                repository.Update(director);
                return RedirectToAction("Index");
            }

            return RedirectToAction("EditDirector", new { directorId = director.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveDirector(int directorId)
        {
            repository.RemoveById(directorId);
            return RedirectToAction("Index");
        }

	}
}