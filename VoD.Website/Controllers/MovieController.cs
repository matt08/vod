﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoD.DAL.DataContexts;
using VoD.Core;

// for GetUserId
using Microsoft.AspNet.Identity;
using VoD.DAL.Services.Interfaces;

namespace VoD.Website.Controllers
{
    public class MovieController : Controller
    {
        private readonly IMovieRepository repository;

        public MovieController(IMovieRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Description(int movieId)
        {
            var description = repository.GetMovieViewModelDetails(movieId);
            return View(description);
        }

        public ActionResult Search(string title)
        {
            var foundMovies = repository.SearchMovies(title);
            if (foundMovies.Count == 0)
            {
                return View("SearchNotFound");
            }

            return View(foundMovies);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Comment(int movieId)
        {
            var movie = repository.GetById(movieId);
            return View(movie);
        }

        [Authorize]
        [HttpPost]
        public ActionResult Comment(Comment movieComment)
        {
            if (ModelState.IsValid)
            {
                var userGuid = User.Identity.GetUserId();
                movieComment.AuthorGuid = Guid.Parse(userGuid);
                repository.AddCommentToMovie(movieComment);
            }

            return RedirectToAction("Description", new { movieId = movieComment.MovieId });
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveComment(int commentId, int movieId)
        {
            repository.RemoveCommentFromMovie(commentId, movieId);
            return RedirectToAction("Description", new { movieId = movieId });
        }
	}
}