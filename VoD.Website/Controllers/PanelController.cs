﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoD.Core;
using VoD.Website.Models;
using System.IO;

namespace VoD.Website.Controllers
{
    [Authorize(Roles="Administrator")]
    public class PanelController : Controller
    {
        //
        // GET: /Panel/
        public ActionResult Index()
        {
            return View();
        }
	}
}