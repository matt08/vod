﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoD.Core;
using VoD.DAL.Services.Interfaces;

namespace VoD.Website.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ManageActorsController : Controller
    {
        private readonly IActorRepository repository;

        public ManageActorsController(IActorRepository repository)
        {
            this.repository = repository;
        }

        //
        // GET: /ManageActors/
        public ActionResult Index()
        {
            var actorsList = repository.GetAll();
            return View(actorsList);
        }

        [HttpGet]
        public ActionResult AddActor()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddActor(Actor actor)
        {
            if (ModelState.IsValid)
            {
                repository.Add(actor);
                return RedirectToAction("Index");
            }

            return View(actor);
        }

        [HttpGet]
        public ActionResult EditActor(int actorId)
        {
            var actor = repository.GetById(actorId);
            return View(actor);
        }

        [HttpPost]
        public ActionResult EditActor(Actor actor)
        {
            if (ModelState.IsValid)
            {
                repository.Update(actor);
                return RedirectToAction("Index");
            }

            return RedirectToAction("EditActor", new { actorId = actor.Id });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RemoveActor(int actorId)
        {
            repository.RemoveById(actorId);
            return RedirectToAction("Index");
        }
	}
}