﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VoD.DAL.Services.Interfaces;
using VoD.Website.Models;

namespace VoD.Website.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMovieRepository repository;

        public HomeController(IMovieRepository repository)
        {
            this.repository = repository;
        }

        public ActionResult Index()
        {
            var latestMovies = repository.GetLatestMovies();
            return View(latestMovies);
        }
    }
}