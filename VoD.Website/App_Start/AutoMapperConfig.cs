﻿
using AutoMapper;
using VoD.Core;
using VoD.Models;
namespace VoD.Website
{
    public class AutoMapperConfig
    {
        public static void CreateMappingDefinitions()
        {
            Mapper.CreateMap<GenreViewModel, Genre>()
                .ForMember(m => m.Id, opt => opt.Ignore())
                .ForMember(m => m.Movies, opt => opt.Ignore());
        }
    }
}