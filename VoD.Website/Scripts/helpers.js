﻿/// <reference path="jquery-1.10.2.js" />

$(function () {
    function confirmDeletion() {
        var response = confirm("Czy na pewno chcesz usunąć?");
        if (response === true) {
            $(this).closest('form').submit();
        }
    }

    $('.removePosition').click(confirmDeletion);
    
    $('#addCommentButton').on('click', function () {
        $('#addCommentForm').toggle();
    });
});