﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VoD.Website.Startup))]
namespace VoD.Website
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
