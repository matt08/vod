﻿using AutoMapper;
using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using VoD.Core;
using VoD.DAL.Services.Interfaces;
using VoD.Models;
using VoD.Website.Controllers;

namespace VoD.Tests
{
    [TestClass]
    public class ManageGenresControllerShould
    {
        IGenreRepository fakeRepository;
        ManageGenresController controller;

        [TestInitialize]
        public void Init()
        {
            fakeRepository = A.Fake<IGenreRepository>();
            controller = new ManageGenresController(fakeRepository);
        }

        [TestMethod]
        public void ReturnModelFromIndex()
        {
            var result = controller.Index() as ViewResult;

            A.CallTo(() => fakeRepository.GetAll()).MustHaveHappened();
            Assert.IsNotNull(result.Model as List<Genre>);
        }

        [TestMethod]
        public void NotReturnModelFromAddGenreByGet()
        {
            var result = controller.AddGenre() as ViewResult;
            Assert.IsNull(result.Model);
        }

        [TestMethod]
        public void ReturnModelFromAddGenreByPostWhenModelIsInvalid()
        {
            controller.ModelState.AddModelError("Key", "Model is invalid.");
            var result = controller.AddGenre(new GenreViewModel()) as ViewResult;
            
            A.CallTo(() => fakeRepository.Add(A<Genre>.Ignored)).MustNotHaveHappened();

            Assert.IsNotNull(result.Model as GenreViewModel);
        }

        [TestMethod]
        public void ReturnToIndexFromAddGenreByPostWhenModelIsValid()
        {
            var validModel = new GenreViewModel { Name = "NewGenre" };
            var result = controller.AddGenre(validModel) as RedirectToRouteResult;
            Assert.IsNotNull(result);
            A.CallTo(() => fakeRepository.Add(A<Genre>.That.Matches(o=>o.Name=="NewGenre"))).MustHaveHappened();
        }

        [TestMethod]
        public void ReturnModelFromEditGenreByGet()
        {
            var result = controller.EditGenre(1) as ViewResult;
            A.CallTo(() => fakeRepository.GetById(1)).MustHaveHappened();
            Assert.IsNotNull(result.Model as Genre);
        }

        [TestMethod]
        public void RedirectToIndexFromEditGenreByPostWhenModelIsValid()
        {
            var validModel = new Genre { Name = "NewGenre" };
            var result = controller.EditGenre(validModel) as RedirectToRouteResult;
            Assert.IsNotNull(result);
            A.CallTo(() => fakeRepository.Update(validModel)).MustHaveHappened();
        }

        [TestMethod]
        public void RedirectToEditActionFromEditGenreByPostWhenModelIsInvalid()
        {
            controller.ModelState.AddModelError("Key", "Model is invalid.");
            var result = controller.EditGenre(new Genre()) as RedirectToRouteResult;
            Assert.IsNotNull(result);
            A.CallTo(() => fakeRepository.Update(new Genre())).MustNotHaveHappened();
        }

        [TestMethod]
        public void CallRepositoryAndRedirectFromRemoveGenre()
        {
            var result = controller.RemoveGenre(1) as RedirectToRouteResult;
            Assert.IsNotNull(result);
            A.CallTo(() => fakeRepository.RemoveById(1)).MustHaveHappened();
        }
    }
}
