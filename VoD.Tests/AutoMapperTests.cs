﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace VoD.Tests
{
    [TestClass]
    public class AutoMapperTests
    {
        [TestMethod]
        public void TestAutoMapperConfiguration()
        {
            VoD.Website.AutoMapperConfig.CreateMappingDefinitions();
            Mapper.AssertConfigurationIsValid();
        }
    }
}
