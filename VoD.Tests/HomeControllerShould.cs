﻿using FakeItEasy;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using VoD.DAL.Services.Interfaces;
using VoD.Website.Controllers;
using VoD.Website.Models;

namespace VoD.Tests
{
    [TestClass]
    public class HomeControllerShould
    {
        [TestMethod]
        public void ReturnModelFromIndex()
        {
            var fakeService = A.Fake<IMovieRepository>();

            A.CallTo(() => fakeService.GetLatestMovies()).Returns(new List<LatestMovieViewModel>());

            var controller = new HomeController(fakeService);
            var result = controller.Index() as ViewResult;

            A.CallTo(() => fakeService.GetLatestMovies()).MustHaveHappened();

            Assert.IsNotNull(result.Model as List<LatestMovieViewModel>);
        }
    }
}
